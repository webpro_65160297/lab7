import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type { User } from "@/types/User";

export const useAuthStore = defineStore("auth", () => {
  function getCurrentUser(): User {
    return {
      id: 1,
      email: "somsak@gmail.com",
      gender: "male",
      fullName: "สมศักดิ์ นานา",
      password: "Passw@rd123",
      roles: ["user"],
    };
  }
  return { getCurrentUser };
});

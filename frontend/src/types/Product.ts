type Product = {
  id: number;
  name: string;
  price: number;
  type: number;
};
function getImageUrl(product: Product) {
  return `/img/coffees/product${product.id}.png`;
}
export { type Product, getImageUrl };

export declare class TemperatureService {
    convert(celsius: number): {
        celsius: number;
        fahrenheit: number;
    };
}
